//
//  CoreDataManager.swift
//  GymManagement
//
//  Created by Predrag Samardzic on 15/08/2017.
//  Copyright © 2017 Predrag Samardzic. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager {
    
    typealias CoreDataManagerCompletion = () -> ()
    
    //  MARK: - Properties
    private let modelName: String
    fileprivate let completion: CoreDataManagerCompletion
    
    lazy var persistentContainer: NSPersistentContainer? = {
        let persistentContainer = NSPersistentContainer(name: modelName)
        persistentContainer.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        return persistentContainer
    }()
    
    //  MARK: - Initialization
    init(modelName: String, completion: @escaping CoreDataManagerCompletion) {
        self.modelName = modelName
        self.completion = completion
        addPersistentContainer()
    }
    
    //  MARK: - Utility
    private func addPersistentContainer() {
        persistentContainer?.loadPersistentStores { _, error in
            if let error = error {
                fatalError("Failed to load Core Data stack: \(error)")
            } 
            else {
                DispatchQueue.main.async { [weak self] in
                    self?.completion()
                }
            }
        }
    }
    
    private func saveChanges() {
        if let viewContext = persistentContainer?.viewContext {
            do {
                if viewContext.hasChanges {
                    try viewContext.save()
                }
            }
            catch {
                let saveError = error as NSError
                print("Unable to save changes of private managed object context.")
                print("\(saveError), \(saveError.localizedDescription)")
            }
        }
    }
    
    public func deleteAll() {
        if let entities = persistentContainer?.managedObjectModel.entities {
            for entity in entities {
                if let name = entity.name {
                    deleteAll(of: name, save: false)
                }
            }
            saveChanges()
        }
    }
    
    private func deleteAll(of entityName: String, save: Bool = true) {
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        
        do {
            try persistentContainer?.viewContext.execute(request)
            if save {
                saveChanges()
            }
        }
        catch {
            print("Unable to delete: \(entityName)")
        }
    }
}
















