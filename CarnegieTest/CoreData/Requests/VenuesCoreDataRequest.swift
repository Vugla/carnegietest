//
//  CommentsCoreDataRequest.swift
//  n1ios
//
//  Created by Predrag Samardzic on 9/15/17.
//  Copyright © 2017 Predrag Samardzic. All rights reserved.


import CoreData

class VenuesCoreDataRequest {
    
    static func getBars(completion: @escaping (([Venue]?, Error?) -> Void)) {
        let context = appDelegate.coreDataManager.persistentContainer!.newBackgroundContext()
        let fetchRequest: NSFetchRequest<Venue> = Venue.fetchRequest()
        do {
            let result = try context.fetch(fetchRequest)
            let venues: [Venue] = result.lazy.compactMap { $0.objectID }.compactMap { appDelegate.coreDataManager.persistentContainer?.viewContext.object(with: $0) as? Venue }
            
            DispatchQueue.main.async {
                completion(venues, nil)
            }
        }
        catch {
            DispatchQueue.main.async {
                completion(nil, error)
            }
        }
        
    }
    
    static func saveArray(venues: [Venue], completion: ((Error?, Bool?) -> Void)? = nil) {
        let privateManagedObjectContext = appDelegate.coreDataManager.persistentContainer!.newBackgroundContext()
        privateManagedObjectContext.performAndWait {
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: Venue.fetchRequest())
            
            do {
                let _ = try privateManagedObjectContext.execute(deleteRequest) as? NSBatchDeleteResult
                try privateManagedObjectContext.save()
            } catch {
                completion?(NSError(domain: "CarnegieTest", code: 600, userInfo: [NSLocalizedDescriptionKey:"Core Data error."]), false)
//                fatalError("Failed to execute request: \(error)")
            }
        }
        privateManagedObjectContext.perform {
            for venue in venues {
                let venueToSave = Venue(context: privateManagedObjectContext)
                venueToSave.address = venue.address
                venueToSave.latitude = venue.latitude
                venueToSave.longitude = venue.longitude
                venueToSave.name = venue.name
            }
            do {
                try privateManagedObjectContext.save()
                completion?(nil, false)
            } catch {
                completion?(NSError(domain: "CarnegieTest", code: 600, userInfo: [NSLocalizedDescriptionKey:"Core Data error."]), false)
//                fatalError("Failure to save context: \(error)")
            }
        }
        
    }

}


