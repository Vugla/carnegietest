//
//  ErrorView.swift
//  mtelBA
//
//  Created by Predrag Samardzic on 6/5/18.
//  Copyright © 2018 Wireless Media. All rights reserved.
//

import UIKit

protocol ErrorViewDelegate: class {
    func tryAgain()
}

class ErrorView: UIView {

    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var bottomLabel: UILabel!
    @IBOutlet weak var tryAgainButton: UIButton!
    weak var delegate: ErrorViewDelegate?
    
    static func view(for title: String?, message: String?, delegate: ErrorViewDelegate?) -> ErrorView {
        let view = Bundle.main.loadNibNamed("ErrorView", owner: self, options: nil)?.first as! ErrorView
        view.delegate = delegate
        view.topLabel.text = title
        view.bottomLabel.text = message
        if let _ = delegate {
            view.tryAgainButton.isHidden = false
        }
        return view
    }
    
    @IBAction func tapOnTryAgain(_ sender: Any) {
        delegate?.tryAgain()
    }
    
}

enum ErrorType {
    case noNet
    case server
    case noBars
}

extension ErrorType {
    var message: String {
        switch self {
        case .noNet:
            return NSLocalizedString("Connection error. Please check your internet connection and try again.", comment: "")
        case .server:
            return NSLocalizedString("Server error. Please try again.", comment: "")
        case .noBars:
            return NSLocalizedString("No bars nearby.", comment: "")
        }
    }
    
}

extension UIView {
    static let errorTag = 696969
    
    func showError(type: ErrorType, delegate: ErrorViewDelegate?) {
        hideError()
        var title = ""
        var message = ""
        switch type {
        case .noNet:
            title = type.message
            message = ""
        case .server:
            title = type.message
            message = ""
        case .noBars:
            title = type.message
            message = ""
        }
        let errorView = ErrorView.view(for: title, message: message, delegate: delegate)
        errorView.tag = UIView.errorTag
        errorView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(errorView)
        bringSubview(toFront: errorView)
        if #available(iOS 11.0, *) {
            errorView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
            errorView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor).isActive = true
            errorView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor).isActive = true
            errorView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor).isActive = true
        } else {
            errorView.topAnchor.constraint(equalTo: topAnchor).isActive = true
            errorView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
            errorView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
            errorView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        }
    }
    
    func hideError() {
        viewWithTag(UIView.errorTag)?.removeFromSuperview()
    }
}
