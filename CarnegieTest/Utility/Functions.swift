//
//  Functions.swift
//  MojTelemach
//
//  Created by Predrag Samardzic on 12/12/16.
//  Copyright © 2016 Predrag Samardzic. All rights reserved.
//

import UIKit
import SystemConfiguration
import Alamofire

func isIpad() -> Bool {
    return UIDevice.current.userInterfaceIdiom == .pad
}

func isLandscape() -> Bool {
    return UIInterfaceOrientationIsLandscape(UIApplication.shared.statusBarOrientation)
}

func screenWidth() -> CGFloat {
    return min(UIScreen.main.bounds.width, UIScreen.main.bounds.height)
}

func screenHeight() -> CGFloat {
    return max(UIScreen.main.bounds.width, UIScreen.main.bounds.height)
}

func isIphoneX() -> Bool {
    return UIDevice.current.userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436
}

func printRequestResponse(defaultResponse: DefaultDataResponse? = nil, responseJson: DataResponse<Any>? = nil, responseData: DataResponse<Data>? = nil) {
    if let response = responseData {
        let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
        print("request headers: \(String(describing: response.request?.allHTTPHeaderFields))")
        print("request: \(String(describing: response.request?.url?.absoluteString))")
        print("response body: \(String(describing: datastring))")
    }
    
    if let response = responseJson {
        let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
        print("request headers: \(String(describing: response.request?.allHTTPHeaderFields))")
        print("request: \(String(describing: response.request?.url?.absoluteString))")
        print("response body: \(String(describing: datastring))")
    }
    
    if let response = defaultResponse {
        let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
        print("request headers: \(String(describing: response.request?.allHTTPHeaderFields))")
        print("request: \(String(describing: response.request?.url?.absoluteString))")
        print("response body: \(String(describing: datastring))")
    }
}

infix operator =~
func =~(string: String, regex: String) -> Bool {
    return string.range(of: regex, options: .regularExpression) != nil
}

//  MARK: - isNilOrEmpty()
protocol _CollectionOrStringish {
    var isEmpty: Bool { get }
}

extension String: _CollectionOrStringish {}
extension Array: _CollectionOrStringish {}
extension Dictionary: _CollectionOrStringish {}
extension Set: _CollectionOrStringish {}

extension Optional where Wrapped: _CollectionOrStringish {
    var isNilOrEmpty: Bool {
        switch self {
        case let .some(value): return value.isEmpty
        default: return true
        }
    }
}
// end: isNilOrEmpty()


