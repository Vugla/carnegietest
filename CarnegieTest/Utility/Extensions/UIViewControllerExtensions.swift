//
//  UIViewControllerExtensions.swift
//  MojTelemach
//
//  Created by Predrag Samardzic on 12/27/16.
//  Copyright © 2016 Predrag Samardzic. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func removeChildVCFromContainerView(_ childVC: UIViewController?) {
        childVC?.willMove(toParentViewController: nil)
        childVC?.view.removeFromSuperview()
        childVC?.removeFromParentViewController()
    }
    
    func addChildVCInsideContainerView(_ childVC: UIViewController, containerView: WMContainerView) {
        self.addChildViewController(childVC)
        containerView.childVC = childVC
        containerView.addSubview(childVC.view)
        childVC.view.translatesAutoresizingMaskIntoConstraints = false
        containerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[childView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["childView":childVC.view]))
        containerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[childView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["childView":childVC.view]))
        childVC.didMove(toParentViewController: self)
    }
}
