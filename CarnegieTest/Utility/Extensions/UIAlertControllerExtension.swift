//
//  UIAlertControllerExtension.swift
//  SportKlub
//
//  Created by Predrag Samardzic on 6/23/16.
//  Copyright © 2016 Wireless Media. All rights reserved.
//

import UIKit

extension UIAlertController {
    
    static func showMessage(_ title:String?, message:String?, inViewController:UIViewController?) {
        if let unwrappedInVC = inViewController {
            let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
            let okAction = UIAlertAction.init(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil)
            alertController.addAction(okAction)
            unwrappedInVC.present(alertController, animated:true, completion: nil)
        }
    }

    static func showMessageWithActions(title:String?, message:String?, inViewController:UIViewController?, actions:[UIAlertAction]) {
        if let unwrappedInVC = inViewController {
            let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
            for action in actions {
                controller.addAction(action)
            }
            unwrappedInVC.present(controller, animated: true)
        }
    }
    
    static func showActionSheetWithActions(title:String?, message:String?, inViewController:UIViewController?, sourceView: UIView?, sourceRect: CGRect?, actions:[UIAlertAction]) {
        if let unwrappedInVC = inViewController {
            let actionSheet = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
            for action in actions {
                actionSheet.addAction(action)
            }
            actionSheet.popoverPresentationController?.sourceView = sourceView
            if let sourceRect = sourceRect {
                actionSheet.popoverPresentationController?.sourceRect = sourceRect
            }
            unwrappedInVC.present(actionSheet, animated: true)
        }
    }
}
