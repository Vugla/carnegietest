//
//  UIViewExtensions.swift
//  SportKlub
//
//  Created by Predrag Samardzic on 6/3/16.
//  Copyright © 2016 Wireless Media. All rights reserved.
//

import UIKit

extension UIView {
    
    func showActivityIndicator(_ style: UIActivityIndicatorViewStyle = .gray) {
        hideActivityIndicator()
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: style)
        activityIndicator.tag = 7777
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        addSubview(activityIndicator)
        bringSubview(toFront: activityIndicator)
        activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        activityIndicator.heightAnchor.constraint(equalToConstant: 25).isActive = true
        activityIndicator.widthAnchor.constraint(equalToConstant: 25).isActive = true
        bringSubview(toFront: activityIndicator)
        activityIndicator.startAnimating()
    }
    
    func hideActivityIndicator() {
        viewWithTag(7777)?.removeFromSuperview()
    }
    
}
