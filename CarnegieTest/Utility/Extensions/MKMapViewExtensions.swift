//
//  MKMapViewExtensions.swift
//  UniFi UG
//
//  Created by Predrag Samardzic on 09/11/2016.
//  Copyright © 2016 Wireless Media. All rights reserved.
//

import MapKit

extension MKMapView {
    
    func setZoomLevel(_ zoomLevel:UInt, center:CLLocationCoordinate2D) {
        setCenterCoordinate(center, zoomLevel: zoomLevel,animated: false)
    }
    
    func zoomLevel() -> Int {
        let MERCATOR_RADIUS:Double = 85445659.44705395
        let MAX_GOOGLE_LEVELS = 20
        let longitudeDelta = region.span.longitudeDelta
        let mapWidthInPixels = bounds.size.width
        let zoomScale = longitudeDelta * MERCATOR_RADIUS * Double.pi / Double(180.0 * mapWidthInPixels)
        var zoomer = Double(MAX_GOOGLE_LEVELS) - log2( zoomScale )
        if ( zoomer < 0 ) {
            zoomer = 0
        } else if zoomer > Double(MAX_GOOGLE_LEVELS - 1) {
            zoomer = Double(MAX_GOOGLE_LEVELS - 1)
        }
        return Int(zoomer) + 1
    }
    
    func bringUserLocationToFront() {
        annotations.forEach {
            if $0 is MKUserLocation {
                if let aView = view(for: $0) {
                    aView.superview?.bringSubview(toFront: aView)
                }
            }
        }
    }
    
    func setCenterCoordinate(_ coordinate:CLLocationCoordinate2D, zoomLevel:UInt, animated:Bool) {
        let span = MKCoordinateSpanMake(0, 360/pow(2, Double(zoomLevel))*Double(frame.size.width)/256)
        setRegion(MKCoordinateRegionMake(coordinate, span), animated: animated)
    }
    
    func displayAnnotations(_ annotations: [MKAnnotation]){
        DispatchQueue.main.async  {
            let allAnnotations = self.annotations
            self.removeAnnotations(allAnnotations)
            self.addAnnotations(annotations)
        }
    }

    func centerMapToCoordinate(_ center:CLLocationCoordinate2D, span:MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)) {
        let region = MKCoordinateRegion(center: center, span: span)
        setRegion(region, animated: true)
    }
    
    func zoomToBounds(ne: CLLocationCoordinate2D, sw: CLLocationCoordinate2D) {
        let nePoint = MKMapPointForCoordinate(ne)
        let swPoint = MKMapPointForCoordinate(sw)
        let neRect = MKMapRectMake(nePoint.x, nePoint.y, 0,0)
        let swRect = MKMapRectMake(swPoint.x, swPoint.y, 0,0)
        let finalRect = MKMapRectUnion(neRect, swRect)
        setVisibleMapRect(finalRect, animated: true)
    }
}
