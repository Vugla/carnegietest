
//  UIStoryboardExtensions.swift
//  MojTelemach
//
//  Created by Predrag Samardzic on 12/2/16.
//  Copyright © 2016 Predrag Samardzic. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    static func venuesStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Venues", bundle: nil)
    }
    
    static func venuesNav() -> UINavigationController {
        return venuesStoryboard().instantiateInitialViewController() as! UINavigationController
    }
    
    static func venuesListVc() -> VenuesListViewController {
        return venuesStoryboard().instantiateViewController(withIdentifier: "VenuesList") as! VenuesListViewController
    }
    
    static func venuesMapVc() -> VenuesMapViewController {
        return venuesStoryboard().instantiateViewController(withIdentifier: "VenuesMap") as! VenuesMapViewController
    }
}

