//
//  PlistManager.swift
//  mtelBA
//
//  Created by Predrag Samardzic on 3/23/18.
//  Copyright © 2018 Wireless Media. All rights reserved.
//

import Foundation

struct PlistKeys {
    static let apiUrl = "apiUrl"
    static let clientId = "foursquareClientId"
    static let clientSecret = "foursquareClientSecret"
    static let barCategoryId = "barCategoryId"
}

class PlistManager {
    
    private static var configPlistDictionary: NSDictionary? {
        get {
            return NSDictionary(contentsOfFile: Bundle.main.path(forResource: "Config", ofType: "plist")!)
        }
    }
    
    private static var developmentDictionary: NSDictionary? {
        get {
            guard let dictionary = configPlistDictionary, let developmentDictionary = dictionary["Development"] as? NSDictionary else {
                return nil
            }
            return developmentDictionary
        }
    }
    
    private static var productionDictionary: NSDictionary? {
        get {
            guard let dictionary = configPlistDictionary, let productionDictionary = dictionary["Production"] as? NSDictionary else {
                return nil
            }
            
            return productionDictionary
        }
    }
    
    static func isProduction() -> Bool {
        guard let dictionary = configPlistDictionary, let enviroment = dictionary["enviroment"] as? String else {
            return false
        }
        
        return enviroment.lowercased() == "production"
    }
    
    static func getValue(for key: String) -> Any? {
        if isProduction() {
            return productionDictionary?[key]
        }
        return developmentDictionary?[key]
    }
}
