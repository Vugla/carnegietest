//
//  LocationManager.swift
//  n1ios
//
//  Created by Predrag Samardzic on 9/22/17.
//  Copyright © 2017 Predrag Samardzic. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: NSObject {
    
    static let sharedInstance = LocationManager()
    private var locationManager = CLLocationManager()
    
    private var clLocationCoordinate: ((CLLocationCoordinate2D?) -> Void)?
    
    override init() {
        super.init()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
    }
    
    func getCurrentLocation(completion: @escaping (CLLocationCoordinate2D?) -> Void) {
        if CLLocationManager.locationServicesEnabled() {
            clLocationCoordinate = completion
            locationManager.requestLocation()
        }
        else {
            completion(nil)
        }
    }
    
    func authorize() {
        locationManager.requestWhenInUseAuthorization()
    }
    
    static func isAuthorizedForLocation() -> Bool {
        return CLLocationManager.authorizationStatus() != .denied && CLLocationManager.authorizationStatus() != .restricted
    }
    
    static func isLocationEnabled() -> Bool {
        return CLLocationManager.locationServicesEnabled()
    }
}

extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            clLocationCoordinate?(location.coordinate)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to find user's location: \(error)")
        clLocationCoordinate?(nil)
    }
}
