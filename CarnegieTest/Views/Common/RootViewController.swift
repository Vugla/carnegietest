//
//  RootViewController.swift
//  GymManagement
//
//  Created by Predrag Samardzic on 27/08/2017.
//  Copyright © 2017 Predrag Samardzic. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {   
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.showActivityIndicator()
        LocationManager.sharedInstance.authorize()
        setupCoreData()
    }
    
    // MARK: - Utility
    private func setupCoreData() {
        if let _ = appDelegate.coreDataManager {
            launchApp()
        } else {
            appDelegate.coreDataManager = CoreDataManager(modelName: "venues") { [weak self] in
                self?.launchApp()
            }
        }
    }
    
    private func launchApp() {
        changeRootViewController(UIStoryboard.venuesNav())
    }
    
    private func changeRootViewController(_ vc:UIViewController!) {
        if let window = appDelegate.window {
            let snapshot:UIView = window.snapshotView(afterScreenUpdates: true)!
            vc.view.addSubview(snapshot)
            let oldVC = window.rootViewController
            window.rootViewController = vc;
            //dismissing old rootViewController to prevent memory usage/leak
            oldVC?.dismiss(animated: false, completion: {
                oldVC?.view?.removeFromSuperview()
            })
            UIView.animate(withDuration: 0.3, animations: {() in
                snapshot.layer.opacity = 0
                snapshot.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5)
            }, completion: { (value: Bool) in
                snapshot.removeFromSuperview()
            });
        }
    }
}

//extension RootViewController: ErrorViewDelegate {
//    func tryAgain() {
//        setupApp()
//    }
//}

