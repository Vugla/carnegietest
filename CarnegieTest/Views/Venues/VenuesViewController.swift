//
//  VenuesViewController.swift
//  CarnegieTest
//
//  Created by Predrag Samardzic on 30/06/2018.
//  Copyright © 2018 Predrag Samardzic. All rights reserved.
//

import UIKit

protocol VenuesProtocol {
    var venues: [Venue] {get set}
}

class VenuesViewController: UIViewController {
    
    @IBOutlet weak var containerView: WMContainerView!
    private lazy var mapButton:UIBarButtonItem = {
        return UIBarButtonItem(image: #imageLiteral(resourceName: "MapIcon"), style: .plain, target: self, action: #selector(tapOnChangeViewButton(_:)))
    }()
    private lazy var listButton: UIBarButtonItem = {
        return UIBarButtonItem(image: #imageLiteral(resourceName: "ListIcon"), style: .plain, target: self, action: #selector(tapOnChangeViewButton(_:)))
    }()
    private var venues = [Venue]() {
        didSet {
            if var vc = containerView.childVC as? VenuesProtocol {
                vc.venues = venues
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        loadData()
    }
    
    private func loadData() {
        view.showActivityIndicator()
        view.hideError()
        navigationItem.rightBarButtonItem?.isEnabled = false
        LocationManager.sharedInstance.getCurrentLocation { [weak self] coordinate in
            //            self?.view.hideActivityIndicator()
            //            if let coordinate = coordinate {
            //            self?.view.showActivityIndicator()
            VenuesRequest.getBars(lat: coordinate?.latitude, lng: coordinate?.longitude, completion: { (venues, error) in
                if let `self` = self {
                    self.view.hideActivityIndicator()
                    self.navigationItem.rightBarButtonItem?.isEnabled = true
                    guard let venues = venues, venues.count > 0, error == nil else {
                        var errorType: ErrorType?
                        if let error = error {
                            switch error._code {
                            case URLError.Code.notConnectedToInternet.rawValue:
                                errorType = .noNet
                            default:
                                errorType = .server
                            }
                        } else {
                            errorType = .noBars
                        }
                        if let errorType = errorType {
                            self.view.showError(type: errorType, delegate: self)
                        }
                        return
                    }
                    if let _ = self.containerView.childVC {} else {
                        self.replaceViewController(in: self.containerView, with: UIStoryboard.venuesListVc())
                    }
                    self.venues = venues
                }
            })
            //            }
            //            else if !LocationManager.isLocationEnabled() {
            //                self?.showAlertController(settingsUrlString: UIApplicationOpenSettingsURLString, message: NSLocalizedString("Please turn on your phone's location. App needs your location in order to show bars nearby.", comment: ""))
            //            }
            //            else if !LocationManager.isAuthorizedForLocation() {
            //                self?.showAlertController(settingsUrlString: UIApplicationOpenSettingsURLString, message: NSLocalizedString("App needs access to your location in order to show bars nearby.", comment: ""))
            //            }
            //            else {
            //                UIAlertController.showMessage(NSLocalizedString("Error", comment: ""), message: NSLocalizedString("We were unable to get your location. Please try again later.", comment: ""), inViewController: self)
            //            }
        }
    }
    
    //    private func showAlertController(settingsUrlString: String, message: String?) {
    //        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel)
    //
    //        let openLocationSettings = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default) { action in
    //            if let url = URL(string: settingsUrlString) {
    //                UIApplication.shared.open(url, options: [:])
    //            }
    //        }
    //
    //        UIAlertController.showMessageWithActions(title: NSLocalizedString("Error", comment: ""), message: message, inViewController: self, actions: [openLocationSettings, cancelAction])
    //    }
    
    @IBAction func tapOnChangeViewButton(_ sender: UIBarButtonItem) {
        if sender == mapButton {
            let vc = UIStoryboard.venuesMapVc()
            vc.venues = venues
            replaceViewController(in: containerView, with: vc)
            navigationItem.rightBarButtonItem = listButton
        } else {
            let vc = UIStoryboard.venuesListVc()
            vc.venues = venues
            replaceViewController(in: containerView, with: vc)
            navigationItem.rightBarButtonItem = mapButton
        }
    }
    
    @IBAction func tapOnRefreshButton(_ sender: Any) {
        loadData()
    }
    
    private func replaceViewController(in containerView: WMContainerView, with viewController: UIViewController) {
        removeChildVCFromContainerView(containerView.childVC)
        addChildVCInsideContainerView(viewController, containerView: containerView)
    }
    
    private func setupSubviews() {
        navigationItem.rightBarButtonItem = mapButton
    }
}

extension VenuesViewController: ErrorViewDelegate {
    func tryAgain() {
        loadData()
    }
}
