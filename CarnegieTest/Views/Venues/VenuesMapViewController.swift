//
//  VenuesMapViewController.swift
//  CarnegieTest
//
//  Created by Predrag Samardzic on 30/06/2018.
//  Copyright © 2018 Predrag Samardzic. All rights reserved.
//

import UIKit
import MapKit

class VenuesMapViewController: UIViewController, VenuesProtocol {
    
    @IBOutlet weak var mapView: MKMapView!
    var venues = [Venue]() {
        didSet {
            displayAnnotations()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        displayAnnotations()
        centerMap()
    }
    
    private func centerMap() {
        LocationManager.sharedInstance.getCurrentLocation { [weak self](coordinate) in
            if let coordinate = coordinate {
                self?.mapView.centerMapToCoordinate(coordinate)
                self?.mapView.setZoomLevel(12, center: coordinate)
            }
        }
    }
    
    private func displayAnnotations() {
        mapView?.displayAnnotations(venues)
    }
    
}

extension VenuesMapViewController: MKMapViewDelegate {
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//        return MKAnnotationView(annotation: annotation, reuseIdentifier: "BarAnnotationView")
//    }
}
