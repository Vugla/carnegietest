//
//  VenueTableViewCell.swift
//  CarnegieTest
//
//  Created by Predrag Samardzic on 30/06/2018.
//  Copyright © 2018 Predrag Samardzic. All rights reserved.
//

import UIKit

class VenueTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    static func cell(for venue: Venue, tableView: UITableView, indexPath: IndexPath) -> VenueTableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VenueTableViewCell", for: indexPath) as! VenueTableViewCell
        
        cell.nameLabel.text = venue.name
        cell.addressLabel.text = venue.address
        return cell
    }

}
