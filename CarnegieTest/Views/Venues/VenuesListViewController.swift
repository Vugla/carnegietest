//
//  VenuesListViewController.swift
//  CarnegieTest
//
//  Created by Predrag Samardzic on 30/06/2018.
//  Copyright © 2018 Predrag Samardzic. All rights reserved.
//

import UIKit

class VenuesListViewController: UIViewController, VenuesProtocol {

    @IBOutlet weak var tableView: UITableView!
    var venues = [Venue]() {
        didSet {
            tableView?.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
    }

    // MARK: - Utility

    private func setupSubviews() {
        tableView.tableFooterView = UIView()
    }
}

extension VenuesListViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return venues.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return VenueTableViewCell.cell(for: venues[indexPath.row], tableView: tableView, indexPath: indexPath)
    }
}
