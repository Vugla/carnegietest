//
//  Request.swift
//  N1
//
//  Created by Predrag Samardzic on 8/28/17.
//  Copyright © 2017 Predrag Samardzic. All rights reserved.
//

import UIKit
import Alamofire

class Request: NSObject {
    
    static let domainName = "CarnegieTest"
    static let defaultErrorCode = 600
    
    static let debugMode = true
    
    static var headers = [String: String]()
    static var baseURL: URL? = URL(string: PlistManager.getValue(for: PlistKeys.apiUrl) as! String)
    static var defaultParameters: [String: Any] = [
        "client_id": PlistManager.getValue(for: PlistKeys.clientId) as! String,
        "client_secret": PlistManager.getValue(for: PlistKeys.clientSecret) as! String,
        "v": "20180323"]
    
    static func getJson(urlString: String, parameters: [String: Any]? = nil, encoding:ParameterEncoding = URLEncoding.default, completion: @escaping (Any?, Error?) -> Void) {
        guard Networking.isConnectedToNetwork() else {
            completion(nil, NSError(domain: domainName, code: URLError.Code.notConnectedToInternet.rawValue, userInfo: nil))
            return
        }
        let parameters = parameters != nil ? defaultParameters.merging(parameters!) { $1 } : defaultParameters
        if let baseURL = baseURL {
            Alamofire.request(baseURL.absoluteString + urlString, parameters: parameters, encoding: encoding, headers: headers).validate().responseJSON { response in
                let parsedData = parseResponseJSON(response)
                completion(parsedData.0, parsedData.1)
            }
        } else {
            completion(nil, NSError(domain: domainName, code: defaultErrorCode, userInfo:[NSLocalizedDescriptionKey:"Url not valid."]))
        }
    }
    
      //  MARK: - Utility
    private static func parseResponseData(_ response: DataResponse<Data>) -> (String?, Error?) {
        if debugMode {
            printRequestResponse(responseData: response)
        }
        switch response.result {
        case .success:
            if let res = response.result.value {
                return (NSString(data: res, encoding: String.Encoding.utf8.rawValue)! as String, nil)
            }
            return ("", NSError(domain: domainName, code: defaultErrorCode, userInfo: nil))
        case .failure(let error):
            if let statusCode = response.response?.statusCode {
                switch statusCode {
                case 401:
                    return ("",  NSError(domain: domainName, code: 401, userInfo: [NSLocalizedDescriptionKey:"Token not valid."]))
                default:
                    return ("", NSError(domain: domainName, code: statusCode, userInfo: [NSLocalizedDescriptionKey:error.localizedDescription]))
                }
            }
            else {
                return ("", NSError(domain: domainName, code: defaultErrorCode, userInfo:[NSLocalizedDescriptionKey:"Timeout."]))
            }
        }
    }
    
    private static func parseStatusCode(_ response: DefaultDataResponse) -> Int {
        if let response = response.response {
            return (response.statusCode)
        } else {
            return defaultErrorCode
        }
    }
    
    private static func parseResponseJSON(_ response: DataResponse<Any>) -> (Any?, Error?) {
        if debugMode {
            printRequestResponse(responseJson: response)
        }
        
        switch response.result {
        case .success:
            if let value = response.result.value {
                return (value as Any, nil)
            }
            return (nil, NSError(domain: domainName, code: defaultErrorCode, userInfo: nil))
        case .failure(let error):
            if let statusCode = response.response?.statusCode {
                switch statusCode {
                case 401:
                    return (nil,  NSError(domain: domainName, code: 401, userInfo:[NSLocalizedDescriptionKey:"Token not valid."]))
                default:
                    return (nil, NSError(domain: domainName, code: statusCode, userInfo:[NSLocalizedDescriptionKey:error.localizedDescription]))
                }
            }
            return (nil, NSError(domain: domainName, code: defaultErrorCode, userInfo:[NSLocalizedDescriptionKey:"Timeout."]))
        }
    }
}

