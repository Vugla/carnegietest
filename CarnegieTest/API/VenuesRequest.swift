//
//  VenuesRequest.swift
//  CarnegieTest
//
//  Created by Predrag Samardzic on 30/06/2018.
//  Copyright © 2018 Predrag Samardzic. All rights reserved.
//

import SwiftyJSON

class VenuesRequest: Request {
    
    static func getBars(lat: Double?, lng: Double?, completion: @escaping ([Venue]?, Error?) -> Void) {
        let path = "venues/search"
        var parameters: [String: Any] = ["categoryId": PlistManager.getValue(for: PlistKeys.barCategoryId) as! String]
        if let lat = lat, let lng = lng {
            parameters["ll"] = "\(lat),\(lng)"
        } else {
            parameters["intent"] = "global"
            parameters["query"] = "bar"
        }
        
        getJson(urlString: path, parameters: parameters) { response, error in
            guard let response = response, error == nil else {
                if let error = error, error._code == URLError.Code.notConnectedToInternet.rawValue {
                    VenuesCoreDataRequest.getBars(completion: { (venues, dataError) in
                        guard let venues = venues, venues.count > 0, dataError == nil else {
                            completion(nil, error)
                            return
                        }
                        completion(venues, nil)
                    })
                } else {
                    completion(nil, error)
                }
                return
            }
            let venues = Venue.array(data: JSON(response)["response"]["venues"])
            VenuesCoreDataRequest.saveArray(venues: venues)
            completion(venues, nil)
        }
    }

}
