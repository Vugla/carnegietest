//
//  Venue+CoreDataClass.swift
//  CarnegieTest
//
//  Created by Predrag Samardzic on 30/06/2018.
//  Copyright © 2018 Predrag Samardzic. All rights reserved.
//
//

import Foundation
import CoreData
import MapKit

@objc(Venue)
public class Venue: NSManagedObject, MKAnnotation {
    public var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    public var title: String? {
        return name
    }
    public var subtitle: String? {
        return address
    }
}
