//
//  Venue+CoreDataProperties.swift
//  CarnegieTest
//
//  Created by Predrag Samardzic on 30/06/2018.
//  Copyright © 2018 Predrag Samardzic. All rights reserved.
//
//

import Foundation
import CoreData
import SwiftyJSON


extension Venue {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Venue> {
        return NSFetchRequest<Venue>(entityName: "Venue")
    }

    @NSManaged public var address: String?
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var name: String?

}

extension Venue {
    func mapFromJson(_ data: JSON) {
        name = data["name"].stringValue
        let location = data["location"]
        longitude = location["lng"].doubleValue
        latitude = location["lat"].doubleValue
        if let addressArray = location["formattedAddress"].arrayObject as? [String] {
            var tmp = ""
            for (index, string) in addressArray.enumerated() {
                tmp += string
                if index < addressArray.count - 1 {
                    tmp += "\n"
                }
            }
            address = tmp
        }
        
    }
    
    static func array(data: JSON) -> [Venue] {
        var retVal = [Venue]()
        for(_, subJson) in data {
            let article = Venue(entity: NSEntityDescription.entity(forEntityName: "Venue", in: appDelegate.coreDataManager.persistentContainer!.viewContext)!, insertInto: nil)
            article.mapFromJson(subJson)
            retVal.append(article)
        }
        return retVal
    }
}
